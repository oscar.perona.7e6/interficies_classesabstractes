import java.util.Scanner

val scanner = Scanner(System.`in`)

abstract class Question(val enunnciado : String, val respuesta: String){
    abstract fun printQuestion()
    abstract fun checkAnswer(userAnswer: String): Boolean
}
class FreeTextQuestion(enunnciado: String, respuesta : String): Question(enunnciado, respuesta){
    override fun printQuestion() {
        println(enunnciado)
    }
    override fun checkAnswer(userAnswer : String): Boolean {
        if ( userAnswer == respuesta){
            println("Resposta correcta")
            return true
        } else {
            println("Resposta incorrecta")
            return false
        }
    }
}
class MultipleChoiseQuestion(enunnciado: String,
                             correctAnswer: String,
                             private val firstIncorrectAnswer : String,
                             private val secondIncorrectAnswer : String,
                             private val thirdIncorrectAnswer : String) : Question(enunnciado, correctAnswer){
    override fun printQuestion()  {
       println(enunnciado)
        val allAnswer = mutableListOf(firstIncorrectAnswer,secondIncorrectAnswer,thirdIncorrectAnswer,respuesta).shuffled()
        println("1-> ${allAnswer[0]}\n" +
                "2-> ${allAnswer[1]}\n" +
                "3-> ${allAnswer[2]}\n" +
                "4-> ${allAnswer[3]}")
    }
    override fun checkAnswer(userAnswer : String): Boolean {
        if ( userAnswer == respuesta){
            println("Resposta correcta")
            return true
        } else {
            println("Resposta incorrecta")
            return false
        }
    }

}
class Quiz() {
    val problems = mutableListOf(
        FreeTextQuestion("Quien pinto la noche estrellada?", "Vincent van Gogh"),
        FreeTextQuestion("De que color es el caballo blanco de Sant Jordi?", "Blanco"),
        FreeTextQuestion("Quien gano la NBA en 2018", "Golden State Warriors"),
        MultipleChoiseQuestion("Cual es el mayor anotador de la historia de la nba", "LeBron James","Kareem Abdul-Jabbar","Michael Jordan","Kobe Bryant"),
        MultipleChoiseQuestion("En que año comenzo la primera guerra mundial", "1914","1814","1923","1918"))
}

fun main(){
    var correctAnswers = 0
    val quiz = Quiz()
    val questions = quiz.problems
    for (question in questions){
        question.printQuestion()
        val userAnswer = readLine()!!
        if (question.checkAnswer(userAnswer)) correctAnswers++
    }
    println()
    println("Respuestas corresctas: $correctAnswers")
}