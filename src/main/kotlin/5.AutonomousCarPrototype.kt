import kotlin.system.exitProcess

enum class Direction {
    FRONT,
    LEFT,
    RIGHT
}
interface CarSensors{
    fun isThereSomethingAt(direction: Direction) : Boolean
    fun go(direction : Direction)
    fun stop()

}
class AutonumousCar(var positionX:Int, var positionY: Int) : CarSensors{
    fun doNextNSteps(n :Int){
        for (i in 1 ..n){
            if (!isThereSomethingAt(Direction.FRONT)){
                go(Direction.FRONT)
            }
            else if (!isThereSomethingAt(Direction.RIGHT)){
                go(Direction.FRONT)
            }
            else if (!isThereSomethingAt(Direction.LEFT)){
                go(Direction.FRONT)
            }
            else stop()
            println("The position of the car is: X:$positionX Y:$positionY")
        }
    }

    override fun isThereSomethingAt(direction: Direction): Boolean {
        return false
    }

    override fun go(direction: Direction) {
        if (direction == Direction.FRONT){
            positionY++
        }
        else if (direction == Direction.RIGHT){
            positionX++
        }
        else {
            positionX--
        }
    }

    override fun stop() {
        println("The position of the car is: X:$positionX Y:$positionY")
        exitProcess(0)
    }
}

fun main(){
    val car = AutonumousCar(0,0)
    car.doNextNSteps(20)

}
