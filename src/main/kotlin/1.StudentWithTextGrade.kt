enum class Grade{
    Suspes,
    Aprobat,
    Be,
    Notable,
    Excelent

}
data class Student(val name: String, val grade: Grade)

fun main (){

    val student1 = Student("Oscar", Grade.Aprobat)
    val student2 = Student("Lorenzo", Grade.Notable)

    println("${student1.name} - ${student1.grade}")
    println("${student2.name} - ${student2.grade}")

}

