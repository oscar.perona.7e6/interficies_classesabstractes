import java.util.Scanner

enum class RainbowColors {
     Rojo,
     Naranje,
     Amarillo,
     Verde,
     Azul,
     Violeta}

fun colorRainbow(color: String ): Boolean {
    return RainbowColors.values().any() { it.name == color }
}
fun main (){
    val scanner = Scanner(System.`in`)
    print("Introdueix un color: ")
    val color = scanner.next()
    println(colorRainbow(color))

}
